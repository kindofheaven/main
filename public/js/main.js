function ResponseForm(day) {
	$(function() {
		$('.response_form_day' + day).show();
		$('.opacity').show();
	})
}
function minusMonth(month) {
	if (month == 2) {
		$('.wa_month_text').text('1 месяц')
		$('.calc_total_month').text('5 600')
		$('.calc_total_period').text('5 600')
		$('button.month_min').addClass('disable')
		$('table.calc_data > tbody > tr:nth-child(2)').remove()
	}
	if (month == 3) {
		$('.wa_month_text').text('2 месяца')
		$('.calc_total_month').text('12 000 ')
		$('.calc_total_period').text('17 600')
		$('table.calc_data > tbody > tr:nth-child(3)').remove()
	}
	if (month == 4) {
		$('.wa_month_text').text('3 месяца')
		$('.calc_total_month').text('20 000  ')
		$('.calc_total_period').text('37 600')
		$('table.calc_data > tbody > tr:nth-child(4)').remove()
	}
	if (month == 5) {
		$('.wa_month_text').text('4 месяца')
		$('.calc_total_month').text('31 200 ')
		$('.calc_total_period').text('68 800 ')
		$('table.calc_data > tbody > tr:nth-child(5)').remove()
	}
	if (month == 6) {
		$('.wa_month_text').text('5 месяцев')
		$('.calc_total_month').text('48 800 ')
		$('.calc_total_period').text('117 600 ')
		$('table.calc_data > tbody > tr:nth-child(6)').remove()
	}
	if (month == 7) {
		$('.wa_month_text').text('6 месяцев')
		$('.calc_total_month').text('79 200 ')
		$('.calc_total_period').text(' 196 800 ')
		$('table.calc_data > tbody > tr:nth-child(7)').remove()
	}
	if (month == 8) {
		$('.wa_month_text').text('7 месяцев')
		$('.calc_total_month').text('130 400 ')
		$('.calc_total_period').text('327 200 ')
		$('table.calc_data > tbody > tr:nth-child(8)').remove()
	}
	if (month == 9) {
		$('.wa_month_text').text('8 месяцев')
		$('.calc_total_month').text('232 800 ')
		$('.calc_total_period').text(' 560 000 ')
		$('table.calc_data > tbody > tr:nth-child(9)').remove()
	}
	if (month == 10) {
		$('.wa_month_text').text('9 месяцев')
		$('.calc_total_month').text('437 600')
		$('.calc_total_period').text('997 600')
		$('table.calc_data > tbody > tr:nth-child(10)').remove()
	}
	if (month == 11) {
		$('.wa_month_text').text('10 месяцев')
		$('.calc_total_month').text('847 200  ')
		$('.calc_total_period').text('1 844 800 ')
		$('table.calc_data > tbody > tr:nth-child(11)').remove()
	}
	if (month == 12) {
		$('.wa_month_text').text('11 месяцев')
		$('.calc_total_month').text('1 666 400 ')
		$('.calc_total_period').text('3 511 200 ')
		$('table.calc_data > tbody > tr:nth-child(12)').remove()
	}
}
function plusMonth(month) {
	if (month == 1) {
		$('button.month_min').removeClass('disable')
		$('.wa_month_text').text('2 месяца')
		$('.calc_total_month').text('12 000 ')
		$('.calc_total_period').text('17 600')
		$('table.calc_data > tbody').append('<tr><td class="month">2</td><td class="partner_start">2</td><td><span class="icon fa fa-plus-circle"></span></td><td class="new_partner">2</td><td class="total_partner">4</td><td><span class="total_month">12 000 ₽</span></td></tr>')
	}
	if (month == 2) {
		$('.wa_month_text').text('3 месяца')
		$('.calc_total_month').text('20 000  ')
		$('.calc_total_period').text('37 600')
		$('table.calc_data > tbody').append('<tr><td class="month">3</td><td class="partner_start">4</td><td><span class="icon fa fa-plus-circle"></span></td><td class="new_partner">4</td><td class="total_partner">8</td><td><span class="total_month">20 000 ₽</span></td></tr>')
	}
	if (month == 3) {
		$('.wa_month_text').text('4 месяца')
		$('.calc_total_month').text('31 200 ')
		$('.calc_total_period').text('68 800 ')
		$('table.calc_data > tbody').append('<tr><td class="month">4</td><td class="partner_start">8</td><td><span class="icon fa fa-plus-circle"></span></td><td class="new_partner">8</td><td class="total_partner">16</td><td><span class="total_month">31 200 ₽</span></td></tr>')
	}
	if (month == 4) {
		$('.wa_month_text').text('5 месяцев')
		$('.calc_total_month').text('48 800 ')
		$('.calc_total_period').text('117 600 ')
		$('table.calc_data > tbody').append('<tr><td class="month">5</td><td class="partner_start">16</td><td><span class="icon fa fa-plus-circle"></span></td><td class="new_partner">16</td><td class="total_partner">32</td><td><span class="total_month">48 800 ₽</span></td></tr>')
	}
	if (month == 5) {
		$('.wa_month_text').text('6 месяцев')
		$('.calc_total_month').text('79 200 ')
		$('.calc_total_period').text(' 196 800 ')
		$('table.calc_data > tbody').append('<tr><td class="month">6</td><td class="partner_start">32</td><td><span class="icon fa fa-plus-circle"></span></td><td class="new_partner">32</td><td class="total_partner">64</td><td><span class="total_month">79 200 ₽</span></td></tr>')
	}
	if (month == 6) {
		$('.wa_month_text').text('7 месяцев')
		$('.calc_total_month').text('130 400 ')
		$('.calc_total_period').text('327 200 ')
		$('table.calc_data > tbody').append('<tr><td class="month">7</td><td class="partner_start">64</td><td><span class="icon fa fa-plus-circle"></span></td><td class="new_partner">64</td><td class="total_partner">128</td><td><span class="total_month">130 400 ₽</span></td></tr>')
	}
	if (month == 7) {
		$('.wa_month_text').text('8 месяцев')
		$('.calc_total_month').text('232 800 ')
		$('.calc_total_period').text(' 560 000 ')
		$('table.calc_data > tbody').append('<tr><td class="month">8</td><td class="partner_start">128</td><td><span class="icon fa fa-plus-circle"></span></td><td class="new_partner">128</td><td class="total_partner">256</td><td><span class="total_month">232 800 ₽</span></td></tr>')
	}
	if (month == 8) {
		$('.wa_month_text').text('9 месяцев')
		$('.calc_total_month').text('437 600')
		$('.calc_total_period').text('997 600')
		$('table.calc_data > tbody').append('<tr><td class="month">9</td><td class="partner_start">256</td><td><span class="icon fa fa-plus-circle"></span></td><td class="new_partner">256</td><td class="total_partner">512</td><td><span class="total_month">437 600 ₽</span></td></tr>')
	}
	if (month == 9) {
		$('.wa_month_text').text('10 месяцев')
		$('.calc_total_month').text('847 200  ')
		$('.calc_total_period').text('1 844 800 ')
		$('table.calc_data > tbody').append('<tr><td class="month">10</td><td class="partner_start">512</td><td><span class="icon fa fa-plus-circle"></span></td><td class="new_partner">512</td><td class="total_partner">1024</td><td><span class="total_month">847 200 ₽</span></td></tr>')
	}
	if (month == 10) {
		$('.wa_month_text').text('11 месяцев')
		$('.calc_total_month').text('1 666 400 ')
		$('.calc_total_period').text('3 511 200 ')
		$('table.calc_data > tbody').append('<tr><td class="month">11</td><td class="partner_start">1024</td><td><span class="icon fa fa-plus-circle"></span></td><td class="new_partner">1024</td><td class="total_partner">2048</td><td><span class="total_month">1 666 400 ₽</span></td></tr>')
	}
	if (month == 11) {
		$('.wa_month_text').text('12 месяцев')
		$('.calc_total_month').text('3 304 800 ')
		$('.calc_total_period').text('6 816 000  ')
		$('button.month_plus').addClass('disable')
		$('table.calc_data > tbody').append('<tr><td class="month">12</td><td class="partner_start">2048</td><td><span class="icon fa fa-plus-circle"></span></td><td class="new_partner">2048</td><td class="total_partner">4096</td><td><span class="total_month">3 304 800 ₽</span></td></tr>')
	}
}
function ezLogin(id) {
	dpd.waclubusersgetit.get(id, function(r,e) {
		console.log(r,e)
		dpd.waclubusersgetit.login({
			username: r.username,
			password: r.ownPass
		}, function(re, er) {
			console.log(re, er)
		})
	})
	window.location.pathname = '/welcome.html'
}

function ToPage() {window.location.pathname = "/page.html"}
function ToLogIn() {window.location.pathname = "/index.html"}
function LogOut() {
	dpd.waclubusersgetit.logout(function(err) {
 			window.location.reload()
 		});
}
function toDeleteDay1(pos) {
		dpd.day1points.get({position: pos}, function(results, error) {
			dpd.day1points.del(results[0].id , function(result, error) {
				for (i = 0; i < (results.length-1); i++) {
					if (results[i].position < pos) {

					} else {
						if (results[i].position > pos) {
							dpd.day1points.put(results[i].id, {
								position: +pos + 1
							})
						}
					}
				}
			})
		})
	}
function toDeleteDay2(pos) {
	dpd.day2points.get({position: pos}, function(results, error) {
			dpd.day2points.del(results[0].id, function(result, error) {
				for (i = 0; i < (results.length-1); i++) {
					if (results[i].position < pos) {
					} else {
						if (results[i].position > pos) {
							dpd.day2points.put(results[i].id, {
								position: +pos + 1
							})
						}
					}
				}
			})
		})
	}
function toDeleteDay3(pos) {
	dpd.day3points.get({position: pos}, function(results, error) {
		dpd.day3points.del(results[0].id , function(result, error) {
			for (i = 0; i < (results.length-1); i++) {
				if (results[i].position < pos) {
					} else {
						if (results[i].position > pos) {
							dpd.day3points.put(results[i].id, {
								position: +pos + 1
							})
						}
					}
				}
			})
		})
	}
if (window.location.pathname === '/admin.html') {
	dpd.waclubusersgetit.me(function(me) {
		if (me.isAdmin !== true && me.username !== 'admin') {
			ToPage()
		}
	})
}
///////////////////////////////////Log in/ Log out////////////////////////////////////////////////////
if (window.location.pathname === '/index.html' || window.location.pathname === '/' || window.location.pathname === '/registration.html') {
	dpd.waclubusersgetit.me(function(me) {
		if (me) {
			ToPage();
		}
	})
} else {
	if (!(window.location.pathname === '/welcome.html' || window.location.pathname === '/registration-continue.html')) {
	dpd.waclubusersgetit.me(function(me) {
		if (!me) {
			ToLogIn();
		}
	})

	}
}




$(document).ready(function() {
	$('#Log_In').click(function() {
	dpd.waclubusersgetit.login({
 		username: $('#reg-email').val(),
  		password: $('#reg-password').val()
	}, function(user, error) {
		console.log($('#reg-email').val(), $('#reg-password').val())
		console.log(user, error)
		if (error) {
			$('#FailedLogIn').show();
		} else {
			window.location.reload()
		}
	});
})
})


///////////////////////////////////////////////////////////////////////////////////////
if (window.location.pathname === '/admin.html') {
	dpd.waclubusersgetit.me(function(me) {
		if (!me.isAdmin) {
			ToPage()
		}
	})
}
$('.tooti > i').hover(function() {
		$($(this).parent().children('p')).show('slow');
	}, function() {
		$($(this).parent().children('p')).hide('slow');
});


$(function() {
	dpd.waclubusersgetit.me(function(me) {
		$('.wa_registration > a').attr('href', 'http://office.vdovgan.ru/join/' + me.idOfCurator)
	})
	$('#FailedLogIn').hide()
	$('.opacity').hide();
	$('.response_form').hide();
	$('.submit_response').click(function() {
		$('.response_form').hide();
		$('.opacity').hide();
	});
	$('.response_form > span').click(function() {
		$('.response_form').hide();
		$('.opacity').hide();
	});

	$('.consult_container').hide();
	$('.consult_button').click(function() {
		$('.opacity').show();
		$('.consult_container').show();
	});
	$('.consult_container > span').click(function() {
		$('.opacity').hide();
		$('.consult_container').hide();
	})
	$('.consult_send').click(function() {
		$('.opacity').hide();
		$('.consult_container').hide();
	})


	$('.consult-container1').hide();
	$('.wa_consult').click(function() {
		$('.consult-container1').show();
	});
	$('.consult-container > span').click(function() {
		$('.consult-container1').hide();
	})

	$('.tooti > p').hide();
	$('.tooti > i').hover(function() {
		$($(this).parent().children('p')).show('slow');
	}, function() {
		$($(this).parent().children('p')).hide('slow');
	});



	dpd.waclubusersgetit.me(function(me) {
		if (me.name === undefined && me.surname === undefined) {
			var name = "Имя"
			var surname = "Фамилия"
		} else {
			if (me.name === undefined) {
				var name = "Имя"
			} else {var name = me.name}
			if (me.surname === undefined) {
				var surname = 'Фамилия'
				} else {var surname = me.surname}
		}
		$('.account-data').append(name + ' ' + surname)
	});


	if (window.location.pathname == '/recovering.html') {
		$(function() {
			$('#recover-result').hide()
			$('.wrong-email').hide()
		})
	};


	if (window.location.pathname == '/registration.html' && window.location.search == '') {
		$(function () {
			$('.registration-success').hide();
			$('.wrong-email').hide()
											$('#wrongPassRes').hide()
		})
	}



	if (window.location.pathname == '/page.html') {
		dpd.day1points.get({$sort: {position: 1}}, function(results, error) {
			for (i = 0; i < results.length; i++) {
				$(function() {
					if (results[i].content.indexOf("<iframe") === 0) {
						$('ul.main-days > li:nth-child(1) > div > ul.day-points').append(
							'<li class="youtube"><i class="fa fa-youtube-play"></i><a href="day1.html">' + results[i].nameOfPoint + '</a></li>'
							)
					} else {
						$('ul.main-days > li:nth-child(1) > div > ul.day-points').append(
							'<li class="book"><i class="fa fa-bookmark"></i><a href="day1.html">' + results[i].nameOfPoint + '</a></li>'
							)
					}
				});
			}
		});

		dpd.day2points.get({$sort: {position: 1}}, function(results, error) {
			for (i = 0; i < results.length; i++) {
				$(function() {
					if (results[i].content.indexOf("<iframe") === 0) {
						$('ul.main-days > li:nth-child(2) > div > ul.day-points').append(
							'<li class="youtube"><i class="fa fa-youtube-play"></i><a href="day2.html">' + results[i].nameOfPoint + '</a></li>'
							)
					} else {
						$('ul.main-days > li:nth-child(2) > div > ul.day-points').append(
							'<li class="book"><i class="fa fa-bookmark"></i><a href="day2.html">' + results[i].nameOfPoint + '</a></li>'
							)
					}
				});
			}
		});

		dpd.day3points.get({$sort: {position: 1}}, function(results, error) {
			for (i = 0; i < results.length; i++) {
				$(function() {
					if (results[i].content.indexOf("<iframe") === 0) {
						$('ul.main-days > li:nth-child(3) > div > ul.day-points').append(
							'<li class="youtube"><i class="fa fa-youtube-play"></i><a href="day3.html">' + results[i].nameOfPoint + '</a></li>'
							)
					} else {
						$('ul.main-days > li:nth-child(3) > div > ul.day-points').append(
							'<li class="book"><i class="fa fa-bookmark"></i><a href="day3.html">' + results[i].nameOfPoint + '</a></li>'
							)
					}
				});
			}
		})
	};
	if (window.location.pathname == '/day1.html') {
		dpd.day1points.get({$sort: {position: 1}}, function(results, error) {
			for (i = 0; i < results.length; i++) {
				$(function() {
					if (results[i].content.indexOf("<iframe") === 0) {
						$('.day-content-n').append(
							'<div class="day-content-item"><h3 class="h3"><i class="fa fa-youtube-play"></i>' + results[i].nameOfPoint + '</h3><div class="day-content-wrap-n">' + results[i].content + '</div></div>'
						)
					} else {
						$('.day-content-n').append(
							'<div class="day-content-item"><h3 class="h3"><i class="fa fa-bookmark"></i>' + results[i].nameOfPoint + '</h3><div class="day-content-wrap-n">' + results[i].content + '</div></div>'
						)
					}
				})
			}
		});
	};
	if (window.location.pathname == '/day2.html') {
		dpd.day2points.get({$sort: {position: 1}}, function(results, error) {
			for (i = 0; i < results.length; i++) {
				$(function() {
					if (results[i].content.indexOf("<iframe") === 0) {
						$('.day-content-n').append(
							'<div class="day-content-item"><h3 class="h3"><i class="fa fa-youtube-play"></i>' + results[i].nameOfPoint + '</h3><div class="day-content-wrap-n">' + results[i].content + '</div></div>'
						)
					} else {
						$('.day-content-n').append(
							'<div class="day-content-item"><h3 class="h3"><i class="fa fa-bookmark"></i>' + results[i].nameOfPoint + '</h3><div class="day-content-wrap-n">' + results[i].content + '</div></div>'
						)
					}
				})
			}
		});
	};
	if (window.location.pathname == '/day3.html') {
		dpd.day3points.get({$sort: {position: 1}}, function(results, error) {
			for (i = 0; i < results.length; i++) {
				$(function() {
					if (results[i].content.indexOf("<iframe") === 0) {
						$('.day-content-n').append(
							'<div class="day-content-item"><h3 class="h3"><i class="fa fa-youtube-play"></i>' + results[i].nameOfPoint + '</h3><div class="day-content-wrap-n">' + results[i].content + '</div></div>'
						)
					} else {
						$('.day-content-n').append(
							'<div class="day-content-item"><h3 class="h3"><i class="fa fa-bookmark"></i>' + results[i].nameOfPoint + '</h3><div class="day-content-wrap-n">' + results[i].content + '</div></div>'
						)
					}
				})
			}
		});
	}
});


dpd.waclubusersgetit.me(function(me) {
	if (me) {
	var months = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31)
	var date = new Date();
	var dateMonth = date.getMonth() + 1;
	var dateDay = date.getDate();
	var dateYear = date.getFullYear();
	var meMonth = me.date.substring(5,7);
	var meMonth = +meMonth;
	var meDay = me.date.substring(8,10);
	var meDay = +meDay;
	var meYear = me.date.substring(0,4);
	var meYear = +meYear;
	if (dateDay === 1) {
		var endOfMonth = months[dateMonth-2]
	}
	if (dateYear === meYear && dateMonth === meMonth && dateDay === meDay) {
		$(function() {
			$('.progress_bar > li:nth-child(2), .progress_bar > li:nth-child(3)').addClass('disabled')
			$('.progress_bar > li:nth-child(2) > a, .progress_bar > li:nth-child(3) > a').removeAttr('href')
			$('.wa_payment').append(
					'<div class="wa-lock-menu"></div><div class="wa-lock-menu-content"><span class="fa fa-lock"></span></div>'
				)
			$('.each-day:nth-child(2)').append(
					'<div class="wa-lock"></div><div class="wa-lock-content"><span class="fa fa-lock"></span><p>Будет доступно завтра</p></div>'
				);
			$('.each-day:nth-child(3)').append(
					'<div class="wa-lock"></div><div class="wa-lock-content"><span class="fa fa-lock"></span><p>Будет доступно послезавтра</p></div>'
				)
			$('.sidebar > li:nth-child(3), .sidebar > li:nth-child(4), .sidebar > li:nth-child(5)').append(
					'<div class="locker"></div><div class="locker-content"><i class="fa fa-lock"></i></div>'
				)
		})
	} else {
		if ((dateYear === meYear && dateMonth === meMonth && dateDay === (meDay+1)) || (dateYear === meYear && dateMonth === (meMonth+1) && dateDay === 1 && meDay === endOfMonth)) {
			$(function() {
				$('.progress_bar > li:nth-child(3)').addClass('disabled')
				$('.progress_bar').addClass('p_bar_2');
				$('.progress_bar > li:nth-child(3) > a').removeAttr('href')
				$('.each-day:nth-child(3)').append(
						'<div class="wa-lock"></div><div class="wa-lock-content"><span class="fa fa-lock"></span><p>Будет доступно завтра</p></div>'
				)
				$('.sidebar > li:nth-child(4), .sidebar > li:nth-child(5)').append(
					'<div class="locker"></div><div class="locker-content"><i class="fa fa-lock"></i></div>'
				)
			})
		} else {
			$(function() {
				$('.progress_bar').addClass('p_bar_3');
			})
		}
	}
	}
})



if (window.location.pathname === '/gifts.html') {
	dpd.wagiftsgetit.get(function(results, err) {
		console.log(results, err)
		for (i = 0; i < results.length; i++) {
			var image = $(results[i].image).html().slice(0, -4)
				$(function() {
					$('.wa_gift_wrapper > ul').append(
							'<li>'+ image +'<h3>' + results[i].name + '</h3><p>' + results[i].uploadText +'<a href="' + results[i].upload + '"> Скачать</a></p></li>'
						)
				})
			}
	})
}




//Photos
if (window.location.pathname === '/photogallery.html') {
dpd.waphotosgetit.get(function(results, err) {
	if (window.location.search === '') {
		for (i = 0; i < results.length; i++) {
			var image = $(results[i].image).html().slice(0, -4)
			console.log(image)
			if (results[i].isGallery) {
				$('.gallery_content').append(
						'<li><div class="gallery_item_container"><a href="?' + results[i].name + '">' + image + '</a><a href="?' + results[i].name + '">' + results[i].name + '</a></div></li>'
					)
			}
		}
	} else {
		var gallery = decodeURIComponent( window.location.search );
		for (i = 0; i < results.length; i++) {
			if (!results[i].isGallery && '?' + results[i].name == gallery) {
			var image = $(results[i].image).html().slice(0, -4)
			$('.left-container').append(image)
			$('.left-container > img').attr('height', '0').attr('float', 'left')
			var sss = $('.left-container > img').attr('src')
			$('.gallery_content').append(
					'<li><div class="gallery_item_container"><a rel="photo" class="tofull" href="' + sss + '">' + image +'</a></div></li>'
				)
			$('.left-container > img').remove()
			}
	}
}})
}
if (window.location.pathname === '/video.html') {
dpd.videoarchive.get(function(results, err) {
	if (window.location.search === '') {
		for (i = 0; i < results.length; i++) {
			if (results[i].isGallery) {
				var image = $(results[i].image).html().slice(0, -4)
				$('.gallery_content').append(
						'<li><div class="gallery_item_container"><a href="?' + results[i].name + '">' + image +'</a><a href="?' + results[i].name + '">' + results[i].name + '</a></div></li>'
					)
			}
		}
	} else {
		var gallery = decodeURIComponent( window.location.search );
		console.log(1)
		for (i = 0; i < results.length; i++) {
			console.log(2)
			if (!results[i].isGallery && '?' + results[i].name == gallery) {
				var image = $(results[i].image).html().slice(0, -4)
			$('.left-container').append(image)
			$('.left-container > img').attr('height', '0').attr('float', 'left')
			var sss = $('.left-container > img').attr('src')
				console.log(3)
				$('.gallery_content').append(
						'<li><div class="gallery_item_container"><a rel="photo" class="gallery" href="#testube">'+ image+ '</a><div id="tube" style="display:none">'+ results[i].video +'</div></div></li>'
					)
			}
	}
}})
}



///Account-page
$(document).ready(function() {
	dpd.waclubusersgetit.me(function(me) {
		if (me.name) {
			$('#nameOn').attr('value', me.name)
		}
		if (me.surname) {
			$('#surnameOn').attr('value', me.surname)
		}
		if (me.phone) {
			$('#phoneOn').attr('value', me.phone)
		}
		if (me.skype) {
			$('#skypeOn').attr('value', me.skype)
		}
		if (me.vk) {
			$('#vkOn').attr('value', me.vk)
		}
		if (me.fb) {
			$('#fbOn').attr('value', me.fb)
		}
		if (me.regnumber) {
			$('#regOn').attr('value', me.regnumber)
		}

		$('.submit-account').click(function() {
			dpd.waclubusersgetit.put(
				me.id, {
					name: 	 	 $('#nameOn').val(),
					surname: 	 $('#surnameOn').val(),
					phone: 	 	 $('#phoneOn').val(),
					skype:   	 $('#skypeOn').val(),
					vk: 	 	 $('#vkOn').val(),
					fb: 	 	 $('#fbOn').val(),
					regnumber: 	 $('#regOn').val(),
					name: 		 $('#nameOn').val()
				}, function(results, error) {
					if (error) return error
				}
			)
			window.location.reload()
		})
	})
})

$(document).ready(function() {
	if (window.location.pathname === '/registration-continue.html') {
		var ref = window.location.search;
		var ref = ref.slice(4);
		var d = new Date().toISOString()
		$('.continune-wrap > .congraz').hide()
		$('#firstStep').click(function() {
			dpd.waclubusersgetit.get(function(results, error) {
				for (var i = 0; i < results.length; i++) {
					if (results[i].id == ref) {
						dpd.curators.get({idOfCurator: results[i].idOfCurator}, function(result, error) {
							console.log(result)
							if ($.isEmptyObject(result)) {
								dpd.curators.get({query: 1}, function(last, error) {
										dpd.waclubusersgetit.put(results[i].id, {
											ref: 0,
											idOfCurator: last[0].idOfCurator
										}, function() {
											dpd.curators.get(function(queryOn, error) {
												for (i = 0; i < queryOn.length; i++) {
													if (queryOn[i].query === 1 && (i+1) !== queryOn.length) {
														dpd.curators.put(queryOn[i].id, {
															query: 0
														})
														dpd.curators.put(queryOn[i+1].id, {
															query: 1
														})
														break
													} else {
														if (queryOn[i].query === 1 && (i+1) == queryOn.length) {
															dpd.curators.put(queryOn[i].id, {
																query: 0
															})
															dpd.curators.put(queryOn[0].id, {
																query: 1
															})
															break
														}
													}
												}
											})
										})
									})
								} else {
									dpd.waclubusersgetit.put(results[i].id, {
										ref: 1
									})
								}
						})
						break
					}
				}
				dpd.waclubusersgetit.get({id: ref}, function(results, error) {
					console.log(results.username, ref)
					dpd.waclubusersgetit.put(
							ref , {
						name: $('#startName').val(),
						phone: $('#startPhone').val(),
						city: $('#startCity').val(),
						date: d
					}, function(r,e) {
						console.log(r,e,$('#startPassword').val())

					})
				})
			$('.continune-wrap > .profile').hide();
			$('.continune-wrap > .congraz').show()
			})
		})
	}
})
$(document).ready(function() {
	dpd.waclubusersgetit.me(function(me) {
		dpd.curators.get({idOfCurator: me.idOfCurator}, function(result, error) {
			var img = $(result[0].image).html().slice(0, -4)
			$('.consult-container > .consult_fio').append(result[0].name + ' ' + result[0].surname);
			$('.consult-container > .img').append(img)
			$('.consult-container > .contacts > ul > li:nth-child(1)').append(result[0].email)
			$('.consult-container > .contacts > ul > li:nth-child(2)').append(result[0].phone)
			$('.consult-container > .contacts > ul > li:nth-child(3)').append(result[0].skype)
			$('.consult-container > .contacts > ul > li:nth-child(4) > a').attr('href', result[0].vk)
			$('.consult-container > .contacts > ul > li:nth-child(5) > a').attr('href', result[0].fb)
		})
	})
})
///
if (window.location.pathname == '/success.html') {
	dpd.success.get(function(results, error) {
		for (i = 0; i < results.length; i++) {
			var image = $(results[i].image).html().slice(0, -4)
			$('.wa_story').append('<li><h2>' + results[i].name + ' ' + results[i].surname +  '</h2><div class="story_body">'+ image + '<table><thead><tr><th></th><th>Сейчас</th></tr></thead><tbody><tr><td>Доход</td><td>' + results[i].income + ' ₽/мес.' + '</td></tr><tr><td>ОБУЧАЕТСЯ В WA</td><td>' + results[i].learn + '</td></tr><tr><td>КОМАНДА WA</td><td>' + results[i].team + '</td></tr><tr><td>ГОРОД</td><td>' + results[i].city + '</td></tr><tr><td>ПОБЫВАЛ В СТРАНАХ</td><td>' + results[i].countries + '</td></tr></tbody></table><p>' + results[i].content + '</p></div></li>')
		}
	})
}

///
if (window.location.pathname == '/admin.html') {
	dpd.day1points.get(function(results, error) {
		for (i = 0; i < results.length; i++) {
			$('.a_day1 > .a_dayprogramms > .a_prog_now').append('<li><a href="day1.html">' + results[i].nameOfPoint + '</a><span class="pos">' + results[i].position + '</span><span class="todelete" onclick="toDeleteDay1(' + results[i].position + ')"><i class="fa fa-close"></i></span></li>')
		}
	})
	dpd.day2points.get(function(results, error) {
		for (i = 0; i < results.length; i++) {
			$('.a_day2 > .a_dayprogramms > .a_prog_now').append('<li><a href="day2.html">' + results[i].nameOfPoint + '</a><span class="pos">' + results[i].position + '</span><span class="todelete" onclick="toDeleteDay2(' + results[i].position + ')"><i class="fa fa-close"></i></span></li>')
		}
	})
	dpd.day3points.get(function(results, error) {
		for (i = 0; i < results.length; i++) {
			$('.a_day3 > .a_dayprogramms > .a_prog_now').append('<li><a href="day3.html">' + results[i].nameOfPoint + '</a><span class="pos">' + results[i].position + '</span><span class="todelete" onclick="toDeleteDay3(' + results[i].position + ')"><i class="fa fa-close"></i></span></li>')
		}
	})
	}

$(document).ready(function() {

if (window.location.pathname == '/admin.html') {
	$(function() {
		$('.a_panels:not(.a_panels.a_program)').hide();
		$('.admin-bar > ul > li').click(function() {
			$('.a_panels').hide()
			$('.' + $(this).attr('id')).show();
		})
		$('#addCuratorPhoto').click(function() {
			var a = $('.a_curators > .summernote').summernote('code')
			var id = $('#idAdding').val()
			dpd.curators.get({idOfCurator: id}, function(result, error) {
				console.log(result)
				dpd.curators.put(result[0].id, {
					image: a
				})
			})
		})
		$('#addSuccessPhoto').click(function() {
			var a = $('.a_success > .summernote').summernote('code')
			var id = $('#Success_Photo').val()
			console.log(id)
				dpd.success.put(id, {
					image: a
				}, function(r, e) {
					console.log(e, r)
				})
		})
		$('#galleryPhoto').click(function() {
			var a = $('.a_photo > .summernote').summernote('code')
			console.log(a)
			var name = $('#nameOfGallery').val()
			dpd.waphotosgetit.get({name: name}, function(result, error) {
				console.log(result)
				dpd.waphotosgetit.put(result[0].id, {
					image: a
				})
			})
		})
		$('#justPhoto').click(function() {
			var a = $('#thisummer > .summernote').summernote('code')
			console.log(a)
			var name = $('#nameOfThis').val()
			dpd.waphotosgetit.post({
					name: name,
					image: a,
					isGallery: false
				}, function(results, error) {
				console.log(results, error)
			})
		})
		$('#addGift').click(function() {
			var a = $('.a_gift > .summernote').summernote('code')
			console.log(a)
			var name = $('#Gift_Photo').val()
			dpd.wagiftsgetit.put(name, {
				image: a
				}, function(results, error) {
				console.log(results, error)
			})
		})
		$('#videoPhoto').click(function() {
			var a = $('.a_video > .summernote').summernote('code')
			var i = $('#Iframe').val()
			console.log(a)
			var name = $('#nameOfVideo').val()
			dpd.videoarchive.get({name: name}, function(result, error) {
				console.log(result)
				dpd.videoarchive.put(result[0].id, {
					image: a,
					video: i
				}, function(r, e) {
					console.log(e, r)
				})
			})
		})
		$('#justVideo').click(function() {
			var a = $('#videosummer > .summernote').summernote('code')
			console.log(a)
			var aa = $('#nameOfThisV').val()
			console.log(name)
			dpd.videoarchive.post({
					name: aa,
					image: a,
					isGallery: false
				}, function(results, error) {
				console.log(results, error)
			})
		})
	})
	$('#a_goDay1').click(function() {
		dpd.day1points.post({
			nameOfPoint: 	  $('#a_dayName1').val(),
			position: $('#a_dayPosition1').val(),
			content:  $('.a_day1 > form > .summernote').summernote('code')
		}, function(results, error) {
			console.log(results, error)
		})
	})
}
})





















