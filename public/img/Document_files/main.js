function ResponseForm(day) {
	$(function() {
		$('.response_form_day' + day).show();
		$('.opacity').show();
	})
}

function ToPage() {window.location.pathname = "/page.html"}
function ToLogIn() {window.location.pathname = "/index.html"}
function LogOut() {
	dpd.waclubusersgetit.logout(function(err) {
 		if (err) {
 			return err;
 		} else {
 			window.location.reload(true)
 		}
	});
}


///////////////////////////////////Log in/ Log out////////////////////////////////////////////////////
if (window.location.pathname === '/index.html') {
	dpd.waclubusersgetit.me(function(me) {
		if (me) {
			ToPage();
		}
	})
} else {
	dpd.waclubusersgetit.me(function(me) {
		if (!me) {
			ToLogIn();
		}
	})
}

$('#Log_In').click(function() {
	dpd.waclubusersgetit.login({
 		username: $('#reg-email').val(),
  		password: $('#reg-passowrd').val()
	}, function(user, error) {
		console.log(user, error)
	});
})










///////////////////////////////////////////////////////////////////////////////////////



// dpd.waclubusersgetit.me(function(me) {
// 	if (me) {
//  	console.log(me);
// } else {
// 	dpd.waclubusersgetit.login({
//  		username: "admin",
//   		password: "fabelwrw1"
// 	}, function(result, error) {
//  		window.location.reload(true);
// 			});
// }
// });


$(function() {
	$('#FailedLogIn').hide()
	$('.opacity').hide();
	$('.response_form').hide();
	$('.submit_response').click(function() {
		$('.response_form').hide();
		$('.opacity').hide();
	});
	$('.response_form > span').click(function() {
		$('.response_form').hide();
		$('.opacity').hide();
	});

	$('.consult_container').hide();
	$('.consult_button').click(function() {
		$('.opacity').show();
		$('.consult_container').show();
	});
	$('.consult_container > span').click(function() {
		$('.opacity').hide();
		$('.consult_container').hide();
	})
	$('.consult_send').click(function() {
		$('.opacity').hide();
		$('.consult_container').hide();
	})


	$('.tooti > p').hide();
	$('.tooti > i').hover(function() {
		$($(this).parent().children('p')).show('slow');
	}, function() {
		$($(this).parent().children('p')).hide('slow');
	});





	// $('.submit-account')




	if (window.location.pathname == '/recovering.html' && window.location.search == '') {
		$(function() {
			$('#recover-result').hide()
		})
	} else {
		if (window.location.pathname == '/recovering.html' && window.location.search !== '') {
			$('#recover-form').hide();
			$('#recover-result').show();
		}
	};


	if (window.location.pathname == '/registration.html' && window.location.search == '') {
		$(function () {
			$('.registration-success').hide();
		})
	} else {
		if (window.location.pathname == '/registration.html' && window.location.search !== '') {
			$(function() {
				$('.registration-success').show();
				$('.recovering').hide();
			})
		}
	}



	if (window.location.pathname == '/page.html') {
		dpd.day1points.get({$sort: {position: 1}}, function(results, error) {
			for (i = 0; i < results.length; i++) {
				$(function() {
					if (results[i].content.indexOf("<iframe") === 0) {
						$('ul.main-days > li:nth-child(1) > div > ul.day-points').append(
							'<li class="youtube"><i class="fa fa-youtube-play"></i><a href="day1.html">' + results[i].nameOfPoint + '</a></li>'
							)
					} else {
						$('ul.main-days > li:nth-child(1) > div > ul.day-points').append(
							'<li class="book"><i class="fa fa-bookmark"></i><a href="day1.html">' + results[i].nameOfPoint + '</a></li>'
							)
					}
				});
			}
		});

		dpd.day2points.get({$sort: {position: 1}}, function(results, error) {
			for (i = 0; i < results.length; i++) {
				$(function() {
					if (results[i].content.indexOf("<iframe") === 0) {
						$('ul.main-days > li:nth-child(2) > div > ul.day-points').append(
							'<li class="youtube"><i class="fa fa-youtube-play"></i><a href="day2.html">' + results[i].nameOfPoint + '</a></li>'
							)
					} else {
						$('ul.main-days > li:nth-child(2) > div > ul.day-points').append(
							'<li class="book"><i class="fa fa-bookmark"></i><a href="day2.html">' + results[i].nameOfPoint + '</a></li>'
							)
					}
				});
			}
		});

		dpd.day3points.get({$sort: {position: 1}}, function(results, error) {
			for (i = 0; i < results.length; i++) {
				$(function() {
					if (results[i].content.indexOf("<iframe") === 0) {
						$('ul.main-days > li:nth-child(3) > div > ul.day-points').append(
							'<li class="youtube"><i class="fa fa-youtube-play"></i><a href="day3.html">' + results[i].nameOfPoint + '</a></li>'
							)
					} else {
						$('ul.main-days > li:nth-child(3) > div > ul.day-points').append(
							'<li class="book"><i class="fa fa-bookmark"></i><a href="day3.html">' + results[i].nameOfPoint + '</a></li>'
							)
					}
				});
			}
		})
	};
	if (window.location.pathname == '/day1.html') {
		dpd.day1points.get({$sort: {position: 1}}, function(results, error) {
			for (i = 0; i < results.length; i++) {
				$(function() {
					if (results[i].content.indexOf("<iframe") === 0) {
						$('.day-content-n').append(
							'<div class="day-content-item"><h3 class="h3"><i class="fa fa-youtube-play"></i>' + results[i].nameOfPoint + '</h3><div class="day-content-wrap-n">' + results[i].content + '</div></div>'
						)
					} else {
						$('.day-content-n').append(
							'<div class="day-content-item"><h3 class="h3"><i class="fa fa-bookmark"></i>' + results[i].nameOfPoint + '</h3><div class="day-content-wrap-n">' + results[i].content + '</div></div>'
						)
					}
				})
			}
		});
	};
	if (window.location.pathname == '/day2.html') {
		dpd.day2points.get({$sort: {position: 1}}, function(results, error) {
			for (i = 0; i < results.length; i++) {
				$(function() {
					if (results[i].content.indexOf("<iframe") === 0) {
						$('.day-content-n').append(
							'<div class="day-content-item"><h3 class="h3"><i class="fa fa-youtube-play"></i>' + results[i].nameOfPoint + '</h3><div class="day-content-wrap-n">' + results[i].content + '</div></div>'
						)
					} else {
						$('.day-content-n').append(
							'<div class="day-content-item"><h3 class="h3"><i class="fa fa-bookmark"></i>' + results[i].nameOfPoint + '</h3><div class="day-content-wrap-n">' + results[i].content + '</div></div>'
						)
					}
				})
			}
		});
	};
	if (window.location.pathname == '/day3.html') {
		dpd.day3points.get({$sort: {position: 1}}, function(results, error) {
			for (i = 0; i < results.length; i++) {
				$(function() {
					if (results[i].content.indexOf("<iframe") === 0) {
						$('.day-content-n').append(
							'<div class="day-content-item"><h3 class="h3"><i class="fa fa-youtube-play"></i>' + results[i].nameOfPoint + '</h3><div class="day-content-wrap-n">' + results[i].content + '</div></div>'
						)
					} else {
						$('.day-content-n').append(
							'<div class="day-content-item"><h3 class="h3"><i class="fa fa-bookmark"></i>' + results[i].nameOfPoint + '</h3><div class="day-content-wrap-n">' + results[i].content + '</div></div>'
						)
					}
				})
			}
		});
	}
});
