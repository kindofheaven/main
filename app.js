var deployd = require('deployd');

var server = deployd({
  port: 5000,
  env: 'production',
  db: {
    host: 'localhost',
    port: 27017,
    name: 'dbname',
    credentials: {
      username: 'dbuser',
      password: 'dbpass'
    }
  }
});

server.listen();

server.on('listening', function() {
  console.log("Server is listening");
});

server.on('error', function(err) {
  console.error(err);
  process.nextTick(function() { 
    process.exit();
  });
});